package com.sourcetable.rocketlaunchers.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

public class CsvUtil {
	
	private CsvUtil() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Read a csv file and import the result to a list, using the csv mapper to convert the data.
	 * @param <T>
	 * @param mapper
	 * @param clazz
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static <T> List<T> read(CsvMapper mapper, Class<T> clazz, String file) throws IOException {
		InputStream stream = new ClassPathResource(file).getInputStream();
		CsvSchema schema = mapper.schemaFor(clazz).withHeader().withColumnReordering(true);
		ObjectReader reader = mapper.readerFor(clazz).with(schema);
		return reader.<T>readValues(stream).readAll();
	}
}
