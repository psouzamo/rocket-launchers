package com.sourcetable.rocketlaunchers.api;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import com.sourcetable.rocketlaunchers.model.company.RocketCompany;
import com.sourcetable.rocketlaunchers.model.company.RocketCompanyService;
import com.sourcetable.rocketlaunchers.model.launch.RocketLaunchService;

import lombok.extern.log4j.Log4j2;

/**
 * Api Controller
 * 
 * Requirements:
 * Your API should return the following data about rocket launches for a given
 * rocket company and/or date range.
 *
 */
@Log4j2
@RestController
public class RocketLaunchersApiController {

	static final String AVG_COSTS = "/avg_costs";
	static final String SUCCESS_MISSIONS = "/success_missions";
	static final String POPULAR_MONTH = "/popular_month";
	static final String TOP3_LOCATIONS = "/top3_locations";
	static final String TOP3_COUNTRIES = "/top3_countries";
	
	RocketLaunchService rocketLaunchService;
	RocketCompanyService rocketCompanyService;

	/**
	 * Constructor
	 * @param rocketLaunchService
	 * @param rocketCompanyService
	 */
	@Autowired
	public RocketLaunchersApiController(RocketLaunchService rocketLaunchService,
			RocketCompanyService rocketCompanyService) {
		this.rocketLaunchService = rocketLaunchService;
		this.rocketCompanyService = rocketCompanyService;
	}
	
	/**
	 * Get Average launch cost (excluding nulls)
	 * 
	 * @return value
	 */
	@GetMapping(AVG_COSTS)
	public BigDecimal getAverageLaunchCosts(//
			@RequestParam(defaultValue = "") Integer company, //
			@RequestParam(defaultValue = "") String fromDate, //
			@RequestParam(defaultValue = "") String toDate) {
		
		// get the company from the param
		RocketCompany rocketCompany = this.validateRocketCompanyParam(company);
		
		// get dates from the params
		LocalDate fromLocalDate = this.validateDateParam(fromDate);
		LocalDate toLocalDate = this.validateDateParam(toDate);
		
		// validate dates
		this.validateDates(fromLocalDate, toLocalDate);
		
		// execute query
		LocalDateTime fromDateTime = fromLocalDate != null ? fromLocalDate.atTime(0, 0) : null;
		LocalDateTime toDateTime = toLocalDate != null ? toLocalDate.atTime(23, 59, 59) : null;
		
		log.info("getAverageLaunchCosts with params -> company: {}, fromDate: {}, toDate: {}", rocketCompany, fromDate, toDate);
		return rocketLaunchService.getAverageLaunchCosts(rocketCompany, fromDateTime, toDateTime);
	}

	/**
	 * Percent of launches where mission_status is success
	 */
	@GetMapping(SUCCESS_MISSIONS)
	public BigDecimal getPercentOfSuccessfullLaunches(//
			@RequestParam(defaultValue = "") Integer company, //
			@RequestParam(defaultValue = "") String fromDate, //
			@RequestParam(defaultValue = "") String toDate) {
		
		// get the company from the param
		RocketCompany rocketCompany = this.validateRocketCompanyParam(company);
		
		// get dates from the params
		LocalDate fromLocalDate = this.validateDateParam(fromDate);
		LocalDate toLocalDate = this.validateDateParam(toDate);
		
		// validate dates
		this.validateDates(fromLocalDate, toLocalDate);
		
		// execute query
		LocalDateTime fromDateTime = fromLocalDate != null ? fromLocalDate.atTime(0, 0) : null;
		LocalDateTime toDateTime = toLocalDate != null ? toLocalDate.atTime(23, 59, 59) : null;
		
		log.info("getPercentOfSuccessfullLaunches with params -> company: {}, fromDate: {}, toDate: {}", rocketCompany, fromDate, toDate);
		return rocketLaunchService.getPercentOfSuccessfulLaunches(rocketCompany, fromDateTime, toDateTime);
	}

	/**
	 * The most popular month for rocket launches
	 */
	@GetMapping(POPULAR_MONTH)
	public String getMostPopularMonthForLaunches(//
			@RequestParam(defaultValue = "") Integer company, //
			@RequestParam(defaultValue = "") String fromDate, //
			@RequestParam(defaultValue = "") String toDate) {
		
		// get the company from the param
		RocketCompany rocketCompany = this.validateRocketCompanyParam(company);
		
		// get dates from the params
		LocalDate fromLocalDate = this.validateDateParam(fromDate);
		LocalDate toLocalDate = this.validateDateParam(toDate);
		
		// validate dates
		this.validateDates(fromLocalDate, toLocalDate);
		
		// execute query
		LocalDateTime fromDateTime = fromLocalDate != null ? fromLocalDate.atTime(0, 0) : null;
		LocalDateTime toDateTime = toLocalDate != null ? toLocalDate.atTime(23, 59, 59) : null;
		
		log.info("getMostPopularMonthForLaunches with params -> company: {}, fromDate: {}, toDate: {}", rocketCompany, fromDate, toDate);
		return rocketLaunchService.getMostPopularMonthForLaunches(rocketCompany, fromDateTime, toDateTime);
	}

	/**
	 * Top three launch_locations
	 */
	@GetMapping(TOP3_LOCATIONS)
	public List<Object[]> getTopThreeLaunchLocations(//
			@RequestParam(defaultValue = "") Integer company, //
			@RequestParam(defaultValue = "") String fromDate, //
			@RequestParam(defaultValue = "") String toDate) {
		
		// get the company from the param
		RocketCompany rocketCompany = this.validateRocketCompanyParam(company);
		
		// get dates from the params
		LocalDate fromLocalDate = this.validateDateParam(fromDate);
		LocalDate toLocalDate = this.validateDateParam(toDate);
		
		// validate dates
		this.validateDates(fromLocalDate, toLocalDate);
		
		// execute query
		LocalDateTime fromDateTime = fromLocalDate != null ? fromLocalDate.atTime(0, 0) : null;
		LocalDateTime toDateTime = toLocalDate != null ? toLocalDate.atTime(23, 59, 59) : null;
		
		log.info("getTopThreeLaunchLocations with params -> company: {}, fromDate: {}, toDate: {}", rocketCompany, fromDate, toDate);
		return rocketLaunchService.getTopThreeLaunchLocations(rocketCompany, fromDateTime, toDateTime);
	}

	/**
	 * Top three countries where launch_locations take place
	 */
	@GetMapping(TOP3_COUNTRIES)
	public List<Object[]> getTopThreeCountriesLaunchLocations(//
			@RequestParam(defaultValue = "") Integer company, //
			@RequestParam(defaultValue = "") String fromDate, //
			@RequestParam(defaultValue = "") String toDate) {
		
		// get the company from the param
		RocketCompany rocketCompany = this.validateRocketCompanyParam(company);
		
		// get dates from the params
		LocalDate fromLocalDate = this.validateDateParam(fromDate);
		LocalDate toLocalDate = this.validateDateParam(toDate);
		
		// validate dates
		this.validateDates(fromLocalDate, toLocalDate);
		
		// execute query
		LocalDateTime fromDateTime = fromLocalDate != null ? fromLocalDate.atTime(0, 0) : null;
		LocalDateTime toDateTime = toLocalDate != null ? toLocalDate.atTime(23, 59, 59) : null;
		
		log.info("getTopThreeCountriesLaunchLocations with params -> company: {}, fromDate: {}, toDate: {}", rocketCompany, fromDate, toDate);
		return rocketLaunchService.getTopThreeCountriesLaunchLocations(rocketCompany, fromDateTime, toDateTime);
	}

	/**
	 * Validate the Rocket Company param
	 * @param company id
	 * @return RocketCompany register
	 */
	private RocketCompany validateRocketCompanyParam(Integer company) {
		// get the company
		RocketCompany rocketCompany = null;
		if (company != null) {
			rocketCompany = rocketCompanyService.findById(company);
			if (rocketCompany == null) {
				// if not exists
				throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Rocket company doesn't exists!");
			}
		}
		return rocketCompany;
	}
	
	/**
	 * Validate the date param
	 * @param date as text
	 * @return LocalDate parsed
	 */
	private LocalDate validateDateParam(String date) {
		if(date == null || date.isBlank()) {
			return null;
		}
		try {
			return LocalDate.parse(date);
		} catch (DateTimeParseException e) {
			// if error parsing
			throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Date invalid!");
		}
	}
	
	/**
	 * Validate the date range
	 * @param fromDate
	 * @param toDate
	 */
	private void validateDates(LocalDate fromDate, LocalDate toDate) {
		if(fromDate != null && toDate != null && fromDate.isAfter(toDate)) {
			// invalid dates
			throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Invalid date range!");
		}
	}
}
