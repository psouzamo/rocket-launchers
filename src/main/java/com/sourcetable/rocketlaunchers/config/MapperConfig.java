package com.sourcetable.rocketlaunchers.config;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.deser.DeserializationProblemHandler;
import com.fasterxml.jackson.databind.deser.ValueInstantiator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.sourcetable.rocketlaunchers.model.company.RocketCompany;
import com.sourcetable.rocketlaunchers.model.location.LaunchLocation;
import com.sourcetable.rocketlaunchers.model.mission.MissionStatus;

@Configuration
public class MapperConfig {

	@Bean
	@Primary
	public ObjectMapper getObjectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		return objectMapper;
	}

	@Bean
	public CsvMapper getCustomCsvMapper() {

		CsvMapper mapper = new CsvMapper();
		mapper.addHandler(new DeserializationProblemHandler() {

			@Override
			public Object handleMissingInstantiator(DeserializationContext ctxt, Class<?> instClass,
					ValueInstantiator valueInsta, JsonParser p, String msg) throws IOException {

				if (instClass.equals(LocalDateTime.class)) {
					// parse date time
					return LocalDateTime.parse(p.getValueAsString(),
							DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
				}
				if (instClass.equals(RocketCompany.class)) {
					// parse rocket company
					RocketCompany rocketCompany = new RocketCompany();
					rocketCompany.setId(p.getValueAsInt());
					return rocketCompany;
				}
				if (instClass.equals(MissionStatus.class)) {
					// parse status
					MissionStatus missionStatus = new MissionStatus();
					missionStatus.setId(p.getValueAsInt());
					return missionStatus;
				}
				if (instClass.equals(LaunchLocation.class)) {
					// location
					LaunchLocation launchLocation = new LaunchLocation();
					launchLocation.setId(p.getValueAsInt());
					return launchLocation;
				}
				return super.handleMissingInstantiator(ctxt, instClass, valueInsta, p, msg);
			}
		});
		return mapper;
	}
}
