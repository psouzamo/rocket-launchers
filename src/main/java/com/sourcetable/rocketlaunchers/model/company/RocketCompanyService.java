package com.sourcetable.rocketlaunchers.model.company;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.sourcetable.rocketlaunchers.util.CsvUtil;

import lombok.extern.log4j.Log4j2;

/**
 * Rocket Company Service.
 *
 */
@Log4j2
@Service
public class RocketCompanyService {

	RocketCompanyRepository repository;
	CsvMapper csvMapper;
	
	@Value("${files.rocket-companies:}")
	private String csvName;
	
	@Autowired
	public RocketCompanyService(RocketCompanyRepository repository, CsvMapper csvMapper) {
		this.repository = repository;
		this.csvMapper = csvMapper;
	}
	
	/**
	 * Import csv file
	 */
	public void importFromCsv() {
		if(csvName == null || csvName.isEmpty()) {
			log.info("No file to import");
			return;
		}
		log.info("Importing file {}", csvName);
		try {
			List<RocketCompany> companies = CsvUtil.read(csvMapper, RocketCompany.class, csvName);
			log.info("Companies: {}", companies);
			repository.saveAll(companies);
		} catch (IOException e) {
			log.error("Error importing csv file.", e);
		}
	}

	/**
	 * Find by Id
	 * @param company
	 * @return
	 */
	public RocketCompany findById(Integer company) {
		if(company == null) {
			return null;
		}
		return repository.findById(company).orElse(null);
	}
}
