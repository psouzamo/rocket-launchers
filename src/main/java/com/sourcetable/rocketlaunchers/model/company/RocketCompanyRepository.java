package com.sourcetable.rocketlaunchers.model.company;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Rocket Company JPA Repository
 *
 */
interface RocketCompanyRepository extends JpaRepository<RocketCompany, Integer> {

}
