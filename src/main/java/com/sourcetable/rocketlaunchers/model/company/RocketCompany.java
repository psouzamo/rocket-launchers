package com.sourcetable.rocketlaunchers.model.company;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * Rocket Company Entity
 *
 */
@Data
@Entity
@Table(name = "rocket_companies")
public class RocketCompany {

	@Id
	private Integer id;
	private String name;
}
