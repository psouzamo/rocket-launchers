package com.sourcetable.rocketlaunchers.model.location;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.sourcetable.rocketlaunchers.util.CsvUtil;

import lombok.extern.log4j.Log4j2;

/**
 * Launch Location Service.
 *
 */
@Log4j2
@Service
public class LaunchLocationService {

	LaunchLocationRepository repository;
	CsvMapper csvMapper;
	
	@Value("${files.launch-locations:}")
	private String csvName;
	
	@Autowired
	public LaunchLocationService(LaunchLocationRepository repository, CsvMapper csvMapper) {
		this.repository = repository;
		this.csvMapper = csvMapper;
	}
	
	public void importFromCsv() {
		if(csvName == null || csvName.isEmpty()) {
			log.info("No file to import");
			return;
		}
		log.info("Importing file {}", csvName);
		try {
			List<LaunchLocation> locations = CsvUtil.read(csvMapper, LaunchLocation.class, csvName);
			log.info("Locations: {}", locations);
			repository.saveAll(locations);
		} catch (IOException e) {
			log.error("Error importing csv file.", e);
		}
	}
}
