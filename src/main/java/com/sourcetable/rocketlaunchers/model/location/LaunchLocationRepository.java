package com.sourcetable.rocketlaunchers.model.location;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Launch Location JPA Repository
 *
 */
interface LaunchLocationRepository extends JpaRepository<LaunchLocation, Integer> {

}
