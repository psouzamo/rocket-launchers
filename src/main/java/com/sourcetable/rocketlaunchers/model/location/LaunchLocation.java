package com.sourcetable.rocketlaunchers.model.location;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

/**
 * Launch Location
 *
 */
@Data
@Entity
public class LaunchLocation {

	@Id
	private Integer id;
	private String location;
}
