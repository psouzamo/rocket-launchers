package com.sourcetable.rocketlaunchers.model.mission;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Mission Status JPA Repository
 *
 */
interface MissionStatusRepository extends JpaRepository<MissionStatus, Integer> {

}
