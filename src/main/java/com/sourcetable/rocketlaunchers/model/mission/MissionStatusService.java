package com.sourcetable.rocketlaunchers.model.mission;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.sourcetable.rocketlaunchers.util.CsvUtil;

import lombok.extern.log4j.Log4j2;

/**
 * Mission Status Service.
 *
 */
@Log4j2
@Service
public class MissionStatusService {

	MissionStatusRepository repository;
	CsvMapper csvMapper;
	
	@Value("${files.mission-status:}")
	private String csvName;
	
	@Autowired
	public MissionStatusService(MissionStatusRepository repository, CsvMapper csvMapper) {
		this.repository = repository;
		this.csvMapper = csvMapper;
	}
	
	public void importFromCsv() {
		if(csvName == null || csvName.isEmpty()) {
			log.info("No file to import");
			return;
		}
		log.info("Importing file {}", csvName);
		try {
			List<MissionStatus> status = CsvUtil.read(csvMapper, MissionStatus.class, csvName);
			log.info("Status: {}", status);
			repository.saveAll(status);
		} catch (IOException e) {
			log.error("Error importing csv file.", e);
		}
	}
}
