package com.sourcetable.rocketlaunchers.model.mission;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

/**
 * Mission Status Entity.
 *
 */
@Data
@Entity
public class MissionStatus {

	@Id
	private Integer id;
	private String status;
}
