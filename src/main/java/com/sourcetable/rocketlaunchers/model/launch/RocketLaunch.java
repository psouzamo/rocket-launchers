package com.sourcetable.rocketlaunchers.model.launch;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.sourcetable.rocketlaunchers.model.company.RocketCompany;
import com.sourcetable.rocketlaunchers.model.location.LaunchLocation;
import com.sourcetable.rocketlaunchers.model.mission.MissionStatus;

import lombok.Data;

/**
 * Rocket Launch Entity.
 *
 */
@Data
@Entity
@Table(name = "rocket_launches")
public class RocketLaunch {

	@Id
	private Integer id;
	
	@JsonAlias("mission_name")
	private String missionName;
	
	@JsonAlias("mission_cost")
	private BigDecimal missionCost;
	
	@JsonAlias("launch_time_date")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime launchTimeDate;
	
	@OneToOne
	@JsonAlias("rocket_company_id")
	private RocketCompany rocketCompany;
	
	@OneToOne
	@JsonAlias("mission_status_id")
	private MissionStatus missionStatus;
	
	@OneToOne
	@JsonAlias("launch_location_id")
	private LaunchLocation launchLocation;
	
}
