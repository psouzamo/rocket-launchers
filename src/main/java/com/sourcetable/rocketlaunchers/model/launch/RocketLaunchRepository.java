package com.sourcetable.rocketlaunchers.model.launch;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sourcetable.rocketlaunchers.model.company.RocketCompany;
import com.sourcetable.rocketlaunchers.model.mission.MissionStatus;

/**
 * Rocket Launch JPA Repository
 *
 */
interface RocketLaunchRepository extends JpaRepository<RocketLaunch, Integer> {

	List<RocketLaunch> findByRocketCompany(RocketCompany company);

	List<RocketLaunch> findByMissionStatus(MissionStatus missionStatus);
}
