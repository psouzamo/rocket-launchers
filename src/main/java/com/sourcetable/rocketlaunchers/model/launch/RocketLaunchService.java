package com.sourcetable.rocketlaunchers.model.launch;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.sourcetable.rocketlaunchers.model.company.RocketCompany;
import com.sourcetable.rocketlaunchers.model.location.LaunchLocation;
import com.sourcetable.rocketlaunchers.util.CsvUtil;

import lombok.extern.log4j.Log4j2;

/**
 * Rocket Launch Service
 *
 */
@Log4j2
@Service
public class RocketLaunchService {

	private static final int MISSION_STATUS_SUCCESS = 2;
	RocketLaunchRepository repository;
	CsvMapper csvMapper;
	EntityManager entityManager;

	@Value("${files.rocket-launches:}")
	private String csvName;

	@Autowired
	public RocketLaunchService(RocketLaunchRepository repository, CsvMapper csvMapper, EntityManager entityManager) {
		this.repository = repository;
		this.csvMapper = csvMapper;
		this.entityManager = entityManager;
	}

	/**
	 * Import csv file.
	 * 
	 */
	public void importFromCsv() {
		if (csvName == null || csvName.isEmpty()) {
			log.info("No file to import");
			return;
		}
		log.info("Importing file {}", csvName);
		try {
			List<RocketLaunch> launches = CsvUtil.read(csvMapper, RocketLaunch.class, csvName);
			log.info("Lauchers: {}", launches);
			repository.saveAll(launches);
		} catch (IOException e) {
			log.error("Error importing csv file.", e);
		}
	}

	/**
	 * Average launch cost (excluding nulls)
	 * @param company
	 * @param fromDate
	 * @param toDate
	 * @return Value of the average costs
	 */
	public BigDecimal getAverageLaunchCosts(RocketCompany company, LocalDateTime fromDate, LocalDateTime toDate) {
		// query
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT AVG(l.missionCost) ");
		queryBuilder.append("FROM RocketLaunch l ");
		queryBuilder.append("WHERE l.missionCost is not null ");
		if (company != null) {
			queryBuilder.append("AND l.rocketCompany = :company ");
		}
		if (fromDate != null) {
			queryBuilder.append("AND l.launchTimeDate >= :fromDate ");
		}
		if (toDate != null) {
			queryBuilder.append("AND l.launchTimeDate <= :toDate ");
		}
		queryBuilder.append("");
		//
		Query query = entityManager.createQuery(queryBuilder.toString());
		if (company != null) {
			query.setParameter("company", company);
		}
		if (fromDate != null) {
			query.setParameter("fromDate", fromDate);
		}
		if (toDate != null) {
			query.setParameter("toDate", toDate);
		}

		Double singleResult = (Double) query.getSingleResult();
		return singleResult != null ? BigDecimal.valueOf(singleResult) : BigDecimal.ZERO;
	}

	/**
	 * Number of launches by mission_status
	 * @param company
	 * @param fromDate
	 * @param toDate
	 * @return List of Mission Status and number of launches
	 */
	public List<Object[]> getNumberOfLaunchesByStatus(RocketCompany company, LocalDateTime fromDate,
			LocalDateTime toDate) {
		// query
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT COUNT(l.id), l.missionStatus.id ");
		queryBuilder.append("FROM RocketLaunch l ");
		queryBuilder.append("WHERE l.missionStatus is not null ");
		if (company != null) {
			queryBuilder.append("AND l.rocketCompany = :company ");
		}
		if (fromDate != null) {
			queryBuilder.append("AND l.launchTimeDate >= :fromDate ");
		}
		if (toDate != null) {
			queryBuilder.append("AND l.launchTimeDate <= :toDate ");
		}
		queryBuilder.append("GROUP BY l.missionStatus.id");
		//
		Query query = entityManager.createQuery(queryBuilder.toString());
		if (company != null) {
			query.setParameter("company", company);
		}
		if (fromDate != null) {
			query.setParameter("fromDate", fromDate);
		}
		if (toDate != null) {
			query.setParameter("toDate", toDate);
		}

		@SuppressWarnings("unchecked")
		List<Object[]> result = (List<Object[]>) query.getResultList();
		return result;
	}
	
	/**
	 * Percent of launches where mission_status is a success
	 * @param company
	 * @param fromDate
	 * @param toDate
	 * @return Percent value of successful launches
	 */
	public BigDecimal getPercentOfSuccessfulLaunches(RocketCompany rocketCompany, LocalDateTime fromDateTime,
			LocalDateTime toDateTime) {
		List<Object[]> numberOfLaunchesByStatus = this.getNumberOfLaunchesByStatus(rocketCompany, fromDateTime, toDateTime);
		
		BigDecimal totalLaunches = BigDecimal.ZERO;
		BigDecimal successLaunches = BigDecimal.ZERO;
		
		for (Object[] objects : numberOfLaunchesByStatus) {
			Long count = (Long) objects[0];
			totalLaunches = totalLaunches.add(new BigDecimal(count));
			Integer status = (Integer) objects[1];
			if(status == MISSION_STATUS_SUCCESS) {
				successLaunches = successLaunches.add(new BigDecimal(count));
			}
		}
		
		// calculate the percent
		return successLaunches.multiply(new BigDecimal(100)).divide(totalLaunches, 2, RoundingMode.CEILING);
	}

	/**
	 * The most popular month for rocket launches
	 * @param company
	 * @param fromDate
	 * @param toDate
	 * @return Name of the mount
	 */
	public String getMostPopularMonthForLaunches(RocketCompany company, LocalDateTime fromDate, LocalDateTime toDate) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT COUNT(l.id), EXTRACT(MONTH FROM l.launchTimeDate) ");
		queryBuilder.append("FROM RocketLaunch l ");
		queryBuilder.append("WHERE l.launchTimeDate is not null ");
		if (company != null) {
			queryBuilder.append("AND l.rocketCompany = :company ");
		}
		if (fromDate != null) {
			queryBuilder.append("AND l.launchTimeDate >= :fromDate ");
		}
		if (toDate != null) {
			queryBuilder.append("AND l.launchTimeDate <= :toDate ");
		}
		queryBuilder.append("GROUP BY EXTRACT(MONTH FROM l.launchTimeDate) ");
		queryBuilder.append("ORDER BY 1 DESC ");
		//
		Query query = entityManager.createQuery(queryBuilder.toString());
		if (company != null) {
			query.setParameter("company", company);
		}
		if (fromDate != null) {
			query.setParameter("fromDate", fromDate);
		}
		if (toDate != null) {
			query.setParameter("toDate", toDate);
		}

		@SuppressWarnings("unchecked")
		List<Object[]> singleResult = query.getResultList();
		Integer object = (Integer) singleResult.get(0)[1];
		return Month.of(object).toString();
	}

	/**
	 * Top three launch_locations
	 * @param company
	 * @param fromDate
	 * @param toDate
	 * @return List of the top 3 launch locations and amount of launches on each one
	 */
	public List<Object[]> getTopThreeLaunchLocations(RocketCompany company, LocalDateTime fromDate,
			LocalDateTime toDate) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT ll, COUNT(l.id) ");
		queryBuilder.append("FROM RocketLaunch l, LaunchLocation ll ");
		queryBuilder.append("WHERE l.launchLocation = ll.id ");
		if (company != null) {
			queryBuilder.append("AND l.rocketCompany = :company ");
		}
		if (fromDate != null) {
			queryBuilder.append("AND l.launchTimeDate >= :fromDate ");
		}
		if (toDate != null) {
			queryBuilder.append("AND l.launchTimeDate <= :toDate ");
		}
		queryBuilder.append("GROUP BY ll.id ");
		queryBuilder.append("ORDER BY 2 DESC ");
		//
		Query query = entityManager.createQuery(queryBuilder.toString());
		if (company != null) {
			query.setParameter("company", company);
		}
		if (fromDate != null) {
			query.setParameter("fromDate", fromDate);
		}
		if (toDate != null) {
			query.setParameter("toDate", toDate);
		}
		query.setMaxResults(3);

		List<LaunchLocation> result = new ArrayList<>();
		@SuppressWarnings("unchecked")
		List<Object[]> singleResult = query.getResultList();
		for (Object[] objects : singleResult) {
			result.add((LaunchLocation) objects[0]);
		}
		return singleResult;
	}

	/**
	 * Top three countries where launch_locations take place
	 * @param company
	 * @param fromDate
	 * @param toDate
	 * @return List with the top 3 Countries and amount of launches on each one
	 */
	public List<Object[]> getTopThreeCountriesLaunchLocations(RocketCompany company, LocalDateTime fromDate,
			LocalDateTime toDate) {
		
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT reverse(substring(reverse(ll.location), 0, position(',' IN reverse(ll.location)))), count(1) ");
		queryBuilder.append("FROM rocket_launches l, launch_location ll ");
		queryBuilder.append("WHERE l.launch_location_id = ll.id ");
		if (company != null) {
			queryBuilder.append("AND l.rocket_company_id = :company ");
		}
		if (fromDate != null) {
			queryBuilder.append("AND l.launch_time_date >= :fromDate ");
		}
		if (toDate != null) {
			queryBuilder.append("AND l.launch_time_date <= :toDate ");
		}
		queryBuilder.append("GROUP BY reverse(substring(reverse(ll.location), 0, position(',' IN reverse(ll.location)))) ");
		queryBuilder.append("ORDER BY 2 DESC ");
		//
		Query query = entityManager.createNativeQuery(queryBuilder.toString());
		if (company != null) {
			query.setParameter("company", company);
		}
		if (fromDate != null) {
			query.setParameter("fromDate", fromDate);
		}
		if (toDate != null) {
			query.setParameter("toDate", toDate);
		}
		query.setMaxResults(3);

		List<String> result = new ArrayList<>();
		@SuppressWarnings("unchecked")
		List<Object[]> singleResult = query.getResultList();
		for (Object[] objects : singleResult) {
			result.add(((String) objects[0]).trim());
		}
		return singleResult;
	}
}
