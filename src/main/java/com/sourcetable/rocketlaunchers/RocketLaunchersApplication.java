package com.sourcetable.rocketlaunchers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import com.sourcetable.rocketlaunchers.model.company.RocketCompanyService;
import com.sourcetable.rocketlaunchers.model.launch.RocketLaunchService;
import com.sourcetable.rocketlaunchers.model.location.LaunchLocationService;
import com.sourcetable.rocketlaunchers.model.mission.MissionStatusService;

import lombok.extern.log4j.Log4j2;

/**
 * Application
 */
@Log4j2
@SpringBootApplication
public class RocketLaunchersApplication {

	@Autowired
	LaunchLocationService launchLocationService;
	@Autowired
	RocketCompanyService rocketCompanyService;
	@Autowired
	MissionStatusService missionStatusService;
	@Autowired
	RocketLaunchService rocketLaunchService;

	/**
	 * Application main
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(RocketLaunchersApplication.class, args);
	}

	/**
	 * Import csv files to the in H2 in-memory database
	 * @param event
	 */
	@EventListener
	public void onApplicationEvent(ApplicationReadyEvent event) {
		log.debug("Importing csv files!");

		// import locations
		launchLocationService.importFromCsv();
		
		// import companies
		rocketCompanyService.importFromCsv();
		
		// import status
		missionStatusService.importFromCsv();
		
		// launches
		rocketLaunchService.importFromCsv();
	}

}
