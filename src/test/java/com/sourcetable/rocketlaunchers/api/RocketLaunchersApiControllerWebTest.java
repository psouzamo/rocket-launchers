package com.sourcetable.rocketlaunchers.api;

import java.math.BigDecimal;
import java.net.URI;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.extern.log4j.Log4j2;

@Log4j2
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RocketLaunchersApiControllerWebTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void getAverageLaunchCostsTest() {
		// average launch costs
		String path = RocketLaunchersApiController.AVG_COSTS;

		// without params
		URI url = getUrl(path);
		BigDecimal averageCosts = restTemplate.getForObject(url, BigDecimal.class);
		log.info(averageCosts);
		Assertions.assertNotNull(averageCosts);

		// with company
		URI filterByCompany = filterByCompany(path);
		BigDecimal averageCostsCompany = restTemplate.getForObject(filterByCompany, BigDecimal.class);
		log.info(averageCostsCompany);
		Assertions.assertNotNull(averageCostsCompany);

		// with dates
		URI filterByDates = filterByDates(path);
		BigDecimal averageCostsDates = restTemplate.getForObject(filterByDates, BigDecimal.class);
		log.info(averageCostsDates);
		Assertions.assertNotNull(averageCostsDates);

		// with company and dates
		URI filterByCompanyAndDates = filterByCompanyAndDates(path);
		BigDecimal averageCostsFilters = restTemplate.getForObject(filterByCompanyAndDates, BigDecimal.class);
		log.info(averageCostsFilters);
		Assertions.assertNotNull(averageCostsFilters);
	}

	@Test
	public void getPercentOfSuccessfullLaunchesTest() {
		// percentual of success launches
		String path = RocketLaunchersApiController.SUCCESS_MISSIONS;

		// without params
		URI url = getUrl(path);
		BigDecimal percentSuccess = restTemplate.getForObject(url, BigDecimal.class);
		log.info(percentSuccess);
		Assertions.assertNotNull(percentSuccess);

		// with company
		URI filterByCompany = filterByCompany(path);
		BigDecimal percentSuccessCompany = restTemplate.getForObject(filterByCompany, BigDecimal.class);
		log.info(percentSuccessCompany);
		Assertions.assertNotNull(percentSuccessCompany);

		// with dates
		URI filterByDates = filterByDates(path);
		BigDecimal percentSuccessDates = restTemplate.getForObject(filterByDates, BigDecimal.class);
		log.info(percentSuccessDates);
		Assertions.assertNotNull(percentSuccessDates);

		// with company and dates
		URI filterByCompanyAndDates = filterByCompanyAndDates(path);
		BigDecimal percentSuccessFilters = restTemplate.getForObject(filterByCompanyAndDates, BigDecimal.class);
		log.info(percentSuccessFilters);
		Assertions.assertNotNull(percentSuccessFilters);
	}

	@Test
	public void getMostPopularMonthForLaunchesTest() {
		// most popular month
		String path = RocketLaunchersApiController.POPULAR_MONTH;

		// without params
		URI url = getUrl(path);
		String popularMonth = restTemplate.getForObject(url, String.class);
		log.info(popularMonth);
		Assertions.assertNotNull(popularMonth);

		// with company
		URI filterByCompany = filterByCompany(path);
		String popularMonthCompany = restTemplate.getForObject(filterByCompany, String.class);
		log.info(popularMonthCompany);
		Assertions.assertNotNull(popularMonthCompany);

		// with dates
		URI filterByDates = filterByDates(path);
		String popularMonthDates = restTemplate.getForObject(filterByDates, String.class);
		log.info(popularMonthDates);
		Assertions.assertNotNull(popularMonthDates);
		// dates are limit in January
		Assertions.assertEquals("JANUARY", popularMonthDates);

		// with company and dates
		URI filterByCompanyAndDates = filterByCompanyAndDates(path);
		String popularMonthFilters = restTemplate.getForObject(filterByCompanyAndDates, String.class);
		log.info(popularMonthFilters);
		Assertions.assertNotNull(popularMonthFilters);
		// date are limit in January
		Assertions.assertEquals("JANUARY", popularMonthFilters);
	}

	@Test
	public void getTopThreeLaunchLocationsTest() {
		// top 3 locations
		String path = RocketLaunchersApiController.TOP3_LOCATIONS;

		// without params
		URI url = getUrl(path);
		List<Object[]> topThreeLaunchLocations = restTemplate.getForObject(url, List.class);
		log.info(topThreeLaunchLocations);
		Assertions.assertNotNull(topThreeLaunchLocations);

		// with company
		URI filterByCompany = filterByCompany(path);
		List<Object[]> topThreeLaunchLocationsCompany = restTemplate.getForObject(filterByCompany, List.class);
		log.info(topThreeLaunchLocationsCompany);
		Assertions.assertNotNull(topThreeLaunchLocationsCompany);

		// with dates
		URI filterByDates = filterByDates(path);
		List<Object[]> topThreeLaunchLocationsDates = restTemplate.getForObject(filterByDates, List.class);
		log.info(topThreeLaunchLocationsDates);
		Assertions.assertNotNull(topThreeLaunchLocationsDates);

		// with company and dates
		URI filterByCompanyAndDates = filterByCompanyAndDates(path);
		List<Object[]> topThreeLaunchLocationsFilters = restTemplate.getForObject(filterByCompanyAndDates, List.class);
		log.info(topThreeLaunchLocationsFilters);
		Assertions.assertNotNull(topThreeLaunchLocationsFilters);
	}

	@Test
	public void getTopThreeCountriesLaunchLocationsTest() {
		// top 3 countries
		String path = RocketLaunchersApiController.TOP3_COUNTRIES;

		// without params
		URI url = getUrl(path);
		List<Object[]> topThreeCountries = restTemplate.getForObject(url, List.class);
		log.info(topThreeCountries);
		Assertions.assertNotNull(topThreeCountries);

		// with company
		URI filterByCompany = filterByCompany(path);
		List<Object[]> topThreeCountriesCompany = restTemplate.getForObject(filterByCompany, List.class);
		log.info(topThreeCountriesCompany);
		Assertions.assertNotNull(topThreeCountriesCompany);

		// with dates
		URI filterByDates = filterByDates(path);
		List<Object[]> topThreeCountriesDates = restTemplate.getForObject(filterByDates, List.class);
		log.info(topThreeCountriesDates);
		Assertions.assertNotNull(topThreeCountriesDates);

		// with company and dates
		URI filterByCompanyAndDates = filterByCompanyAndDates(path);
		List<Object[]> topThreeCountriesFilters = restTemplate.getForObject(filterByCompanyAndDates, List.class);
		log.info(topThreeCountriesFilters);
		Assertions.assertNotNull(topThreeCountriesFilters);
	}
	
	private URI getUrl(String path) {
		return this.getUrl(path, null);
	}

	private URI getUrl(String path, MultiValueMap<String, String> params) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("http://localhost:" + port);
		builder.path(path);
		if (params != null) {
			builder.queryParams(params);
		}
		return builder.encode().build().toUri();
	}

	private URI filterByCompany(String path) {
		LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("company", "15");
		return getUrl(path, params);
	}

	private URI filterByDates(String path) {
		LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("fromDate", "2020-01-01");
		params.add("toDate", "2020-01-31");
		return getUrl(path, params);
	}

	private URI filterByCompanyAndDates(String path) {
		LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("company", "15");
		params.add("fromDate", "2020-01-01");
		params.add("toDate", "2020-01-31");
		return getUrl(path, params);
	}
}
