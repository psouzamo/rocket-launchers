# Rocket Launchers API Implementation

This project is a solution implemented for the *Rocket Launches Code Simulation* described [here](https://github.com/sourcetable/rocket-launch-api-simulation).


## Running the API

For running the API, we have some options:

1) Run on a java environment (jdk11+):

```
./mvnw package -DskipTests && java -jar target/rocket-launchers-0.0.1-SNAPSHOT.jar
```

2) Run on a docker environment:

```
docker build -t rocket-launchers .
docker run -p 8080:8080 rocket-launchers
```

## Running Tests

Running the tests

1) Run on a java environment (jdk11+):

```
./mvnw test
```

## API Specs

That is a REST API and all methods are HTTP `GET`. 

### Filtering

All methods have the possibility to pass the following query parameters for filtering:  

* `company` --> company id of a rocket company.  
Ex: `15` (for SpaceX)  

* `fromDate` --> the start date for the query.  
Ex: `2010-01-01` (for launches after 2010-01-01 00:00:00)  

* `toDate` --> the end date for the query.  
Ex: `2020-01-31` (for launches before 2020-01-31 23:59:59)  


The filtering parameters are not required and there is validation on those if the user enters the wrong value.


### Methods

1) Average launch cost (excluding nulls): `/avg_costs`

```
curl -X GET "http://localhost:8080/avg_costs"
# or with parameters
curl -X GET "http://localhost:8080/avg_costs?company=15&fromDate=2010-01-01&toDate=2020-01-31"
```

2) Percent of launches where mission_status is a success: `/success_missions`

```
curl -X GET "http://localhost:8080/success_missions"
# or with parameters
curl -X GET "http://localhost:8080/success_missions?company=15&fromDate=2010-01-01&toDate=2020-01-31"
```

3) The most popular month for rocket launches: `/popular_month`

```
curl -X GET "http://localhost:8080/popular_month"
# or with parameters
curl -X GET "http://localhost:8080/popular_month?company=15&fromDate=2010-01-01&toDate=2020-01-31"
```

4) Top three launch_locations: `/top3_locations`

```
curl -X GET "http://localhost:8080/top3_locations"
# or with parameters
curl -X GET "http://localhost:8080/top3_locations?company=15&fromDate=2010-01-01&toDate=2020-01-31"
```

5) Top three countries where launch_locations take place: `/top3_countries`

```
curl -X GET "http://localhost:8080/top3_countries"
# or with parameters
curl -X GET "http://localhost:8080/top3_countries?company=15&fromDate=2010-01-01&toDate=2020-01-31"
```

For more details, please access the swagger-ui: `http://localhost:8080/swagger-ui/`


## Implementation

### Technology

As the simulation could be written in any language/framework, I've chosen to implement in Java(Spring Boot) for the following reasons:

1) Performance

As that is a public API, performance to return the data is very important.

2) Easy Implementation / Add-ons

Spring Boot has an easy and reliable way to create REST APIs, connect to a database (JPA), and other features that are easy to add in, as caching (important for this just query API), security (if it needs to be added), etc.

3) Tests

Spring has also a good toolbox for tests, which are very important.


### Details

For the implementation, first I've created the project using `https://start.spring.io/`, which creates a boilerplate project.  
I added the basic libraries to make the API work. Please check the `pom.xml` for more info.

A few mentionable points:
 
#### JPA/Postgresql

Using the JPA to map and execute the queries in the database make life a lot easier.

#### Swagger UI

I've added Swagger UI to describe and show details of the API, allowing an easy way to access/test the API.  
More info about `https://swagger.io/tools/swagger-ui/`

When the application is working, we can easily access it at:  
`http://localhost:8080/swagger-ui/`


### Considerations

Few points for consideration:

#### Queries

For this simulation, all the methods execute a query in the database.  
Those queries are proper sql/jpa queries and those queries can be easily cached.

The solution for `Top three countries where launch_locations take place`, was done using a native query for Postgresql.  
That is not ideal, but it is one way to implement it.  

In my opinion, the proper solution for that would be creating a new field on the launch_locations table, to save the country.  
That would avoid making the complex query to get the results.

#### Tests

Just a few tests were implemented for this simulation.  
I could have created a few more to get at least 85% coverage.


